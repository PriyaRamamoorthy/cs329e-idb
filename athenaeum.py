#-----------------------------------------
# app2.py
# creating first flask application
#-----------------------------------------
from flask import Flask, render_template


app = Flask(__name__)

@app.route('/')
def home():
	return render_template('home.html')

@app.route('/books/')
def books():
	return render_template('books.html')

@app.route('/about/')
def about():
        return render_template('about.html')

@app.route('/emperor/')
def emperor():
        return render_template('emperor.html')

@app.route('/four_hour/')
def four_hour():
        return render_template('four_hour.html')

@app.route('/gardens_of_moon/')
def gardens_of_moon():
        return render_template('gardens_of_moon.html')

@app.route('/harry_potter/')
def harry_potter():
        return render_template('harry_potter.html')

@app.route('/palmac/')
def palmac():
        return render_template('palmac.html')

@app.route('/pottermore/')
def pottermore():
        return render_template('pottermore.html')

@app.route('/publishers/')
def publishers():
        return render_template('publishers.html')

@app.route('/randomhouse/')
def randomhouse():
        return render_template('randomhouse.html')

@app.route('/simonandschuster/')
def simonandschuster():
        return render_template('simonandschuster.html')

@app.route('/steven_erikson/')
def steven_erikson():
        return render_template('steven_erikson.html')

@app.route('/timothy_ferriss/')
def timothy_ferriss():
        return render_template('timothy_ferriss.html')

@app.route('/j_k_rowling/')
def j_k_rowling():
        return render_template('j_k_rowling.html')

@app.route('/siddhartha_mukherjee/')
def siddhartha_mukherjee():
        return render_template('siddhartha_mukherjee.html')

@app.route('/authors/')
def authors():
        return render_template('authors.html')

if __name__ == "__main__":
	app.run()
#----------------------------------------
# end of app2.py
#----------------------------------------
